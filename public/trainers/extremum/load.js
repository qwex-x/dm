const canvas = document.querySelector('canvas');

function FIELD(canvas, graph, layout, highlight) {
  const container = canvas.parentElement;
  const context   = canvas.getContext('2d');
  let _highlight = new Set(highlight)
  
  const radius = 20;
  
  function resize(){
    container.style.width  = (width  * radius * 2)+"px";
    container.style.height = (height * radius * 2)+"px";
    
    canvas.width = canvas.parentElement.offsetWidth;
    canvas.height = canvas.parentElement.offsetHeight;
  }
  
  let height = 0;
  let width  = 0;
  for (let node in layout) {
    width  = Math.max((layout[node].x + 1), width);
    height = Math.max((layout[node].y + 1), height);
  }

  
  function calculateXY(nodeLayout) {
    return [
      (nodeLayout.x * 2 + 1) * radius,
      (2*(height  - nodeLayout.y) - 1) * radius
    ]
  }
  
  function drawGraph() {
    resize()
    
    context.clearRect(0, 0, canvas.width, canvas.height);
    
    for (let node in layout) {
      const [x, y] = calculateXY(layout[node]);

      context.beginPath();
      context.fillStyle = graph.isSelected(node)?"#ff0000":(_highlight.has(Number(node))?"#006600":"#0066cc");
      //#ff0000
      context.arc(x, y, radius, 0, Math.PI * 2, true);
      context.strokeStyle = "#009999";
      context.stroke();
      context.fill();

      let text = String(node);
      // const textHeight = 30;
      context.font = radius * 2 * 0.8 + "px serif";
      const textWidth  = context.measureText(text).width / 2;
      const textHeight = context.measureText(text).actualBoundingBoxAscent / 2
      context.fillStyle = "#00ff66";
      context.fillText(text, x - textWidth, y + textHeight);
    }
    for (let edge of graph.edges) {
      const [x1, y1] = calculateXY(layout[edge[0]]);
      const [x2, y2] = calculateXY(layout[edge[1]]);

      context.beginPath();
      context.strokeStyle = '#000000';
      context.moveTo(x1, y1 - radius);
      context.lineTo(x2, y2 + radius);
      context.stroke();
    }
  }
  
  function findNode(canvasX ,canvasY) {
    for (let node in layout) {
      const [x, y] = calculateXY(layout[node])
      
      if ((Math.pow(x - canvasX, 2) + Math.pow(y - canvasY, 2)) <= Math.pow(radius, 2) )
        return node;
    }
    return undefined;
  }

  function getCursorPosition(e) {
    const rect = canvas.getBoundingClientRect()
    const x = e.clientX - rect.left
    const y = e.clientY - rect.top
    return [x, y];
  }
  
  canvas.onclick = function (e) {
    graph.pick(findNode(...getCursorPosition(e)));
    drawGraph()
  }
  //
  // drawGraph();
  
  this.drawGraph = drawGraph;
  
  this.selected = function (selected) {
    graph.selected(new Set(selected));
    drawGraph();
  }
  
  this.highlight = function (highlight) {
    _highlight = new Set(highlight)
    drawGraph();
  }
}

function ADJACENCY(nodes, edges) {
  let index = new Map();
  const matrix = nodes.map(
    function(node, i){
      index.set(node, i)
      return Array(nodes.length).fill(false)
    } 
  )
  for (let edge of edges) {
    if (index?.has(edge[0]) && index?.has(edge[1])) 
      matrix[index.get(edge[0])][index.get(edge[1])] = true
  }
  nodes.forEach((_, i) => {
      matrix[i][i] = true
      nodes.forEach((_, j) =>
        nodes.forEach((_, k) => {
            matrix[j][k] = matrix[j][k] || (matrix[j][i] && matrix[i][k])
          }
        )
      )
    }
  )
  
  function printMatrix() {
    console.log(matrix.map(m => m.map(i => i ? 1 : 0).join(" ")).join("\n"));
  }

  this.less = function(node1, node2) {
    if (index?.has(node1) && index?.has(node2)) {
      return matrix[index.get(node1)][index.get(node2)]
    }
  }
}

function GRAPH(nodes, edges) {
  const _this = this
  this.nodes = nodes;
  this.edges = edges;

  const adjacency = new ADJACENCY(nodes, edges)
   
  let selected = new Set();
  
  this.pick = function(node) {
    if (node !== undefined) {
      node = Number(node)
      if (selected.has(node))
        selected.delete(node)
      else
        selected.add(node)
      fireChange();
    }
  }
  this.isSelected = function(node) {
    node = Number(node);
    return selected.has(node);
  }
  
  function _maximals(selected){
    const maximals = [];
    for (let node1 of selected) {
      if (![...selected].some(function (node2) {
        return node2 !== node1 && adjacency.less(node1, node2)
      })) maximals.push(node1);
    }
    return maximals;
  }
  
  this.maximals = function(_selected) {
    return _maximals(_selected?new Set(_selected):selected)
  }
  
  function _minimals(selected) {
    
    const minimals = [];
    for (let node1 of selected) {
      if (![...selected].some(function (node2) {
        return node2 !== node1 && adjacency.less(node2, node1)
      })) minimals.push(node1);
    }
    return minimals;
  }

  this.minimals = function(_selected) {
    return _minimals(_selected?new Set(_selected):selected)
  }
  
  function _greatest(selected) {
    const maximals = _this.maximals(selected)
    if (maximals.length === 1)
      return maximals
    else
      return [];
  }
  this.greatest = function(_selected) {
    return _greatest(_selected?new Set(_selected):selected)
  }
  function _least(selected) {
    const minimals = _minimals(selected)
    if (minimals.length === 1)
      return minimals
    else
      return [];
  }
  this.least = function(_selected) {
    return _least(_selected?new Set(_selected):selected)
  }

  this.majorants = function(_selected){
    _selected = _selected?new Set(_selected):selected
    const majorants = [];
    for (let node1 of nodes)
      if ([..._selected].every(function (node2) {
        return node1 === node2 || adjacency.less(node2, node1)
      })) majorants.push(node1)
    return majorants;
  }
  this.minorants = function(_selected) {
    _selected = _selected?new Set(_selected):selected
    const minorants = [];
    for (let node1 of nodes)
      if ([..._selected].every(function (node2) {
        return node1 === node2 || adjacency.less(node1, node2)
      })) minorants.push(node1)
    return minorants;
  }
  
  this.supremum = function(_selected) {
    _selected = _selected?new Set(_selected):selected
    return _least(new Set(_this.majorants(_selected)))
  }
  this.infinum = function(_selected) {
    _selected = _selected?new Set(_selected):selected
    return _greatest(new Set(_this.minorants(_selected)))
  }
  
  this.selected = function(newSelected) {
    if (newSelected !== undefined) {
      selected = new Set(newSelected);
    } else 
    return selected;
  }
  
  let _handler = function(){};
  this.onChange = function(handler) {
    _handler = handler;
  }
  
  function fireChange(){
    _handler();
  }
}

let graph = new GRAPH(
  [1,2,3,4,5,6,7,8,9,10,11,12,13],
  [
    [ 1, 2],
    [ 2, 3], [ 2, 4], [ 2, 5],
    [ 3, 6], [ 3, 7], [ 4, 7], [ 4, 8], [ 5, 8],
    [ 6, 9], [ 6,10], [ 7,10], [ 8,10], [ 8,11],
    [ 9,12], [10,12], [10,13], [11,13],
  ]
);
  
[7,3,1].forEach(function(node){graph.pick(node)})
const layout = {
  1 : {x: 2, y: 0},
  2 : {x: 2, y: 2},
  3 : {x: 0, y: 4},
  4 : {x: 2, y: 4},
  5 : {x: 4, y: 4},
  6 : {x: 0, y: 6},
  7 : {x: 2, y: 6},
  8 : {x: 4, y: 6},
  9 : {x: 0, y: 8},
  10: {x: 2, y: 8},
  11: {x: 4, y: 8},
  12: {x: 1, y: 10},
  13: {x: 3, y: 10}
}
let field = new FIELD(
  canvas, graph, layout
);

let initialState = {
  graph: graph,
  layout: layout,
  highlight: [],
  instruction: "Выберите от 3 до 5 вершин и нажмите Далее",
  inputs: {},
  selected: []
}

field.drawGraph();

function APP() {

  
  function STAGE({name, instruction, showAnswers, buildState, checkState, persistState}) {
    const _this = this
    this.buildState = function (state) {
      if (buildState !== undefined) {
        return buildState({...state})
      } else return state
    }
    this.name = name ?? "";
    
    this.checkState = function (state) {
      if (checkState !== undefined) {
        return checkState(state)
      } else return '';
    }
    
    
    this.run = function (state) {
      state = _this.buildState(state)
      
      if (instruction !== undefined)
        setInstruction(instruction)
      setField(state)
      setInfos(state)
      printInputs(document.getElementById('inputs'), state.inputs )
      if (showAnswers) {
        const answers   = buildAnswers(graph, state.highlight)
        const container = document.getElementById('answers')  
        printInputs(container, answers)
      }
    }
    this.persistState = function(state) {
      if (buildState !== undefined) {
        state = buildState({...state})
      }
      state = {...state, selected: graph.selected()};
      if (persistState !== undefined) {
        return persistState(state)
      } return state
      //return {...state, selected: graph.selected()}
    }
  }
  
  function updateState(state){
    return {...state, }
  }
  
  function setInfos(infos) {
    const container = document.getElementById('infos')
  }
  
  function setInstruction(instruction) {
    document.getElementById('instruction').textContent = instruction
  }
  function setField(state) {
    field.selected(state.selected)
    field.highlight(state.highlight)
  }
  
  const labels = {
    "maximals" : "МАКС.",
    "minimals" : "МИН. ",
    "greatest" : "НАИБ.",
    "least"    : "НАИМ.",
    "majorants": "ВГ.  ",
    "minorants": "НГ.  ",
    "supremum" : "СУПР.",
    "infinum"  : "INF. ",
  }
  
  function printInputs(container, inputs) {
    container.innerHTML = '';
    for (let label in labels) {
      if (inputs[label]) {
        let str = Array.isArray(inputs[label])?`{${inputs[label].join(", ")}}`:inputs[label]

        container.innerHTML =
          container.innerHTML +
          '<pre>' + labels[label] + ' = '+str+'</pre>';
      }
    }
  }
  
  function buildAnswers(graph, selected) {
    let answers = {};
    for (let label in labels) {
      answers[label] = graph[label](selected)
    }
    return answers;
  }
  
  function selectSubset(initState) {
    let state = {...initState}
    const instruction = "Выберите от 3 до 5 вершин и нажмите Далее"
    
    this.run = function() {
      setInstruction(instruction)
      setField(state.selected)  
    }
    this.nextState = function() {
      state = {...state, highlight: state.graph.selected()}
      return state;
    }
  } 
  
  let stageSelectSubset = new STAGE({
    instruction: "Выберите от 3 до 5 вершин и нажмите Далее",
  })
  let stageSelectMaximals = new STAGE({
    name: "МАКС",
    instruction: "Выберите максимальные элементы",
    buildState: function (state) {
      return {...clearSelected('maximals')(state), highlight: state.selected}
    },
    checkState: function (state) {
      if (state.selected.size < 3)
        return "Выбрано меньше 3-х элементов!";
      if (state.selected.size > 5)
        return "Выбрано больше 5 элементов";
      return "";
    },
    persistState: writeInput('maximals')
  })
  let stageSelectMinimals = new STAGE({
    instruction: "Выберите минимальные элементы",
    buildState: clearSelected('minimals'),
    persistState: writeInput('minimals')
  })
  let stageSelectGreatest = new STAGE({
    instruction: "Выберите наибольший элемент",
    buildState: clearSelected('greatest '),
    persistState: writeInput('greatest')
  })
  let stageSelectLeast = new STAGE({
    instruction: "Выберите наименьший элемент",
    buildState: clearSelected('least'),
    persistState: writeInput('least')
  })
  let stageSelectMajorants= new STAGE({
    instruction: "Выберите верхние грани (мажоранты)",
    buildState: clearSelected('majorants'),
    persistState: writeInput('majorants')
  })
  let stageSelectMinorants = new STAGE({
    instruction: "Выберите нижние грани (миноранты)",
    buildState: clearSelected('minorants'),
    persistState: writeInput('minorants')
  })
  let stageSelectSupremum = new STAGE({
    instruction: "Выберите точную верхнюю грань",
    buildState: clearSelected('supremum'),
    persistState: writeInput('supremum')
  })
  let stageSelectInfinum = new STAGE({
    instruction: "Выберите точную нижнюю грань",
    buildState: clearSelected('infinum'),
    persistState: writeInput('infinum')
  })
  let stageAnswers = new STAGE({
    instruction: "Правильные ответы",
    showAnswers: true
  })
  
  function clearSelected(label) {
    return state => {
      const newState = {...state, selected: []};
      delete newState.inputs[label];
      
      return newState
    }
  }
  
  function writeInput(label) {
    return state => {
      let newState = {...state}
      newState.inputs[label] = [...state.selected]
      return newState
    }
  }
  

  
  const states = [initialState]
  let step = 0;
  const stages = [
    stageSelectSubset,
    stageSelectMaximals,
    stageSelectMinimals,
    stageSelectGreatest,
    stageSelectLeast,
    stageSelectMajorants,
    stageSelectMinorants,
    stageSelectSupremum,
    stageSelectInfinum,
    stageAnswers
  ]
  
  this.run = function() {
    let stage = stages[step];
    let state = states[step];
    
    let check = stage.checkState(state)
    if (check !== "") {
      document.getElementById('error').innerText = check;
      if (step > 0) {
        --step
        states.pop()
      }
    } else {
      document.getElementById('error').innerText = "";
      stage.run(state)
    }
    
    
    
  }

  const _this = this
  document.getElementById('nextBtn').onclick = function() {
    if (step < stages.length - 1) {
      let stage = stages[step];
      let state = stage.persistState(states[step])
      states[step] = state
      states.push(state)
      ++step
      _this.run()
    }
  }

}

let app = new APP();

app.run();